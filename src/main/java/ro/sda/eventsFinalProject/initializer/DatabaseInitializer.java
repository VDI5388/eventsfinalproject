package ro.sda.eventsFinalProject.initializer;


import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ro.sda.eventsFinalProject.model.Category;
import ro.sda.eventsFinalProject.model.Event;
import ro.sda.eventsFinalProject.service.CategoryService;
import ro.sda.eventsFinalProject.service.EventService;

import java.time.LocalDateTime;

@Component
public class DatabaseInitializer {

    private final EventService eventService;
    private final CategoryService categoryService;

    public DatabaseInitializer(EventService eventService, CategoryService categoryService) {
        this.eventService = eventService;
        this.categoryService=categoryService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void initDataBase(){

        Category g1 = new Category(null, "Festivale de muzica", null);
        Category g2 = new Category(null, "Art and paintings", null);
        Category g3 = new Category(null, "Zilele orasului", null);

        categoryService.saveCategory(g1);
        categoryService.saveCategory(g2);
        categoryService.saveCategory(g3);


        Event e1 = new Event (null,
                "UNTOLD",
                LocalDateTime.of(2023, 8, 11, 20 ,0,0),
                LocalDateTime.of(2023,8, 14, 23, 59, 00),
                "Epic music",
                "Cluj-Napoca",
                "https://s.inyourpocket.com/img/text/romania/cluj-napoca/top-10-events-cluj-napoca.jpg", g1
               );

        Event e2 = new Event (null,
                "Electric Castle",
                LocalDateTime.of(2023, 9, 11, 20 ,0,0),
                LocalDateTime.of(2023,9, 14, 23, 59, 00),
                "Festival de muzica",
                "Cluj-Napoca",
                "https://cdn.pixabay.com/photo/2019/08/13/08/48/bucuresti-4402855_1280.jpg", g1
        );

        Event e3 = new Event (null,
                "Instalatie de arta contemporana",
                LocalDateTime.of(2023, 1, 1, 8 ,0,0),
                LocalDateTime.of(2023,12, 31, 23, 59, 00),
                "Arts and culture",
                "Timisoara",
                "https://static.wixstatic.com/media/51e2ac_047876e2d5444974ad99b9ca726a072a~mv2.jpg/v1/fill/w_250,h_327,al_c,q_90,enc_auto/51e2ac_047876e2d5444974ad99b9ca726a072a~mv2.jpg", g2
        );



        Event e4 = new Event (null,
                "Sarbatoarea Castanelor",
                LocalDateTime.of(2023, 9, 27, 8 ,0,0),
                LocalDateTime.of(2023,9, 30, 23, 59, 00),
                "Concert si foc de artificii",
                "Baia Mare",
                "https://tinyurl.com/mvpk5u97", g3
        );

        Event e5 = new Event (null,
                "Zidurile Cetatii",
                LocalDateTime.of(2023, 6, 28, 20 ,0,0),
                LocalDateTime.of(2023,9, 06, 23, 59, 00),
                "Festival de muzica",
                "Timisoara",
                "https://tinyurl.com/mvpk5u97", g3
        );

        Event e6 = new Event (null,
                "Raliul Timisoarei",
                LocalDateTime.of(2024, 8, 10, 10 ,0,0),
                LocalDateTime.of(2024,8, 10, 13, 00, 00),
                "Raliul Iulius Town",
                "Timisoara",
                "https://www.cm-braga.pt/archive/cache/img/sz800x600/start007_1500.jpg", g3
        );

        eventService.saveEvent(e1);
        eventService.saveEvent(e2);
        eventService.saveEvent(e3);
        eventService.saveEvent(e4);
        eventService.saveEvent(e5);
        eventService.saveEvent(e6);

    }

}
